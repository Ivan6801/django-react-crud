import React from "react";
import ReactDOM from "react-dom/client";
import AppRouter from "./routes/index.jsx";
import CamerasProvider from "./context/CamerasProvider.jsx";
import "./index.css";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <CamerasProvider>
      <AppRouter />
    </CamerasProvider>
  </React.StrictMode>
);
