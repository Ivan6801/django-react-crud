import { createContext } from "react";

const CamerasContext = createContext({});

export default CamerasContext;
