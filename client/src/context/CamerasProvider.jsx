/* eslint-disable react/prop-types */
import { useEffect, useReducer } from "react";
import CamerasContext from "./CamerasContext";
import CamerasReducer from "../reducers/CamerasReducer";
import { getAllTasks } from "../api/tasks.api";

const CamerasProvider = ({ children }) => {
  const [state, dispatch] = useReducer(CamerasReducer);

  const getCameras = async () => {
    try {
      const { data } = await getAllTasks();
      dispatch({ type: "SET_CAMERAS", payload: data });
    } catch (e) {
      dispatch({ type: "SET_ERROR", payload: "Something went wrong." });
      console.error(e);
    }
  };

  useEffect(() => {
    getCameras();
  }, []);

  return (
    <CamerasContext.Provider value={{ state, dispatch }}>
      {children}
    </CamerasContext.Provider>
  );
};

export default CamerasProvider;
