import { Fragment } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Form } from "../pages/Form";
import { AlertProvider } from "../components/AlertProvider";
import App from "../pages/Home/App";

function AppRouter() {
  return (
    <Fragment>
      <AlertProvider>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<App />} />
            <Route path="/form" element={<Form />} />
          </Routes>
        </BrowserRouter>
      </AlertProvider>
    </Fragment>
  );
}
export default AppRouter;
