import { BasicTable } from "../Table";
import "./MainDash.css";

export function MainDash() {
  return (
    <div className="MainDash">
      <h1>Dashboard</h1>
      <BasicTable />
    </div>
  );
}
