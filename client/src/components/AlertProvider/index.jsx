import PropTypes from "prop-types";

import { SnackbarProvider } from "notistack";

export const AlertProvider = ({ children }) => (
  <SnackbarProvider
    maxSnack={3}
    anchorOrigin={{
      horizontal: "center",
      vertical: "top",
    }}
    preventDuplicate
    style={{
      whiteSpace: "pre-line",
    }}
  >
    {children}
  </SnackbarProvider>
);

AlertProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
