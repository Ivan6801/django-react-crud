import { useState } from "react";
import { Button } from "../Button";
import { useCameras } from "../../hooks/useCameras";
import { CamerasItem } from "../CamerasItem";
import { Spinner } from "../Spinner";
import { Table } from "../Table";
import { Modal } from "../Modal";

export function CamerasList() {
  const { state } = useCameras();
  const [showModal, setShowModal] = useState(false);

  return (
    <>
      {state && state.loading && <Spinner />}

      <div className="flex flex-col items-end mt-5">
        {state && !state.error && (
          <Button label="New camera" click={() => setShowModal(!showModal)} />
        )}
      </div>
      <Table>
        {state &&
          state.cameras?.map((item) => {
            return <CamerasItem key={item.id} camera={item} />;
          })}

        {state && !state.cameras.length && (
          <tr className="bg-white">
            <td colSpan={7} className="py-4 px-6 text-sm text-center">
              <p className="font-semibold">There are no cameras. 😢</p>
            </td>
          </tr>
        )}
      </Table>

      {state && state.error && (
        <div className="flex items-center justify-center h-89v">
          <p className="font-bold text-red-500 text-xl">
            {state && state.error} API 😢
          </p>
        </div>
      )}

      {showModal && <Modal onClose={() => setShowModal(!showModal)} />}
    </>
  );
}
