import React, { useState } from "react";
import { useCameras } from "../../hooks/useCameras";
import { useForm } from "../../hooks/useForm";
// import { addCamera, editCamera } from "../services";
import { Button } from "../Button";
import { FileInput } from "../FileInput";
import { RawModal } from "../RawModal";
import { TextInput } from "../TextInput";

export const Modal = ({ onClose, isEdit, item }) => {
  const [isValid, setIsValid] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);
  const [error, setError] = useState(false);
  const [imgPath, setImgPath] = useState({});
  const [imgPreview, setImgPreview] = useState("");
  const [valueTask, setValueTask] = useState({
    title: "",
    description: "",
    done: false,
    image: null,
  });

  const { dispatch } = useCameras();

  const handlerChange = (e) => {
    setValueTask(e.target.value);
  };

  // const { values, handlerChange } =
  //   useForm <
  //   CameraDTO >
  //   {
  //     name: isEdit ? item.name : "",
  //     model: isEdit ? item.model : "",
  //     brand: isEdit ? item.brand : "",
  //     connection_type: isEdit ? item.connection_type : "WIFI",
  //     price: isEdit ? item.price : "",
  //   };

  // const { name, model, brand, connection_type, price } = values;

  const handlerAdd = async (e) => {
    e.preventDefault();

    if (
      name !== "" &&
      model !== "" &&
      brand !== "" &&
      price !== "" &&
      imgPath instanceof File
    ) {
      setBtnLoading(true);
      setIsValid(false);
      setError(false);

      // const { error, dataCamera } = await addCamera(values, imgPath);

      // if (!error) {
      //   dispatch({ type: "ADD_CAMERA", payload: dataCamera });
      //   onClose();
      //   return;
      // }

      setError(true);
      setBtnLoading(false);
      return;
    }

    setIsValid(true);
    setError(false);
  };

  const handlerEdit = async (e) => {
    e.preventDefault();

    if (
      name !== "" &&
      model !== "" &&
      brand !== "" &&
      price !== "" &&
      imgPath instanceof File
    ) {
      setBtnLoading(true);
      setIsValid(false);
      setError(false);

      // const { error, dataCamera } = await editCamera(
      //   item?._id!,
      //   values,
      //   imgPath
      // )

      // if (!error) {
      //   dispatch({
      //     type: 'EDIT_CAMERA',
      //     payload: { ...item!, ...dataCamera }
      //   })

      onClose();
      return;
    }

    setError(true);
    setBtnLoading(false);
    return;
    setIsValid(true);
    setError(false);
  };

  return (
    <RawModal onClose={onClose}>
      <form
        onSubmit={isEdit ? handlerEdit : handlerAdd}
        className="flex flex-col -mt-4 p-5 space-y-4 lg:px-8 sm:pb-6 xl:pb-8"
      >
        <h3 className="text-xl text-center font-medium text-gray-600">
          {isEdit ? "Edit camera" : "Add a new Camera"}
        </h3>

        <TextInput
          name="title"
          placeholder="Title"
          value={valueTask.title}
          onChange={handlerChange}
        />
        <FileInput name="image" imgSelected={imgPreview}>
          <input
            className="h-full w-full opacity-0"
            type="file"
            name="image"
            onChange={({ target }) => {
              if (target.files && target.files[0]) {
                setImgPath(target.files[0]);

                // Select the image
                const reader = new FileReader();
                reader.onload = () => {
                  setImgPreview(reader.result);
                };

                reader.readAsDataURL(target.files[0]);
              }
            }}
          />
        </FileInput>

        {/* Validation and Errors */}
        {/* {isValid && (
          <div className="flex items-center justify-end">
            <p className="text-red-500 text-sm font-bold">
              All fields are required.
            </p>
          </div>
        )}

        {error && (
          <div className="flex items-center justify-end">
            <p className="text-red-500 text-sm font-bold">
              Something went wrong.
            </p>
          </div>
        )} */}

        <hr />

        {/* Footer */}
        <div className="flex items-center justify-end space-x-2">
          <Button label="Close" isClose click={onClose} />
          <Button
            label={btnLoading ? "Loading..." : isEdit ? "Edit" : "Add"}
            isSubmit
            isLoading={btnLoading}
          />
        </div>
      </form>
    </RawModal>
  );
};
