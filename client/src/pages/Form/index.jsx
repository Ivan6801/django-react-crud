import { useState } from "react";
import { createTasks } from "../../api/tasks.api";
import { useSnackbar } from "notistack";

export function Form() {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [done, setDone] = useState(false);
  const [image, setImage] = useState(null);

  const { enqueueSnackbar } = useSnackbar();

  const handleImageChange = (e) => {
    const selectedImage = e.target.files[0];
    setImage(selectedImage);
  };

  const handleSubmitForm = async (e) => {
    e.preventDefault();

    if (!title || !description || !image) {
      enqueueSnackbar(
        "Los campos de título, descripción y foto son obligatorios.",
        {
          variant: "warning",
          anchorOrigin: {
            horizontal: "center",
            vertical: "top",
          },
          preventDuplicate: true,
        }
      );
      return;
    }

    const formData = new FormData();
    formData.append("image", image);
    formData.append("title", title);
    formData.append("description", description);
    formData.append("done", done);

    await createTasks(formData)
      .then((response) => {
        enqueueSnackbar(response.statusText, {
          variant: "success",
          anchorOrigin: {
            horizontal: "center",
            vertical: "top",
          },
          preventDuplicate: true,
        });

        console.log(response);
      })
      .catch((error) => {
        enqueueSnackbar(error, {
          variant: "error",
          anchorOrigin: {
            horizontal: "center",
            vertical: "top",
          },
          preventDuplicate: true,
        });
      });

    setTitle("");
    setDescription("");
    setDone(false);
    setImage(null);
  };

  return (
    <div className="App">
      <div className="AppGlass">
        <br />
        <form onSubmit={handleSubmitForm}>
          <input name="image" type="file" onChange={handleImageChange} />
          <input
            type="text"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            placeholder="Título"
          />
          <input
            type="text"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            placeholder="Descripción"
          />
          <label>
            <input
              type="checkbox"
              checked={done}
              onChange={(e) => setDone(e.target.checked)}
            />
            Hecho
          </label>
          <div>
            {image && (
              <div>
                <p>Imagen seleccionada:</p>
                <img
                  width={30}
                  height={30}
                  src={URL.createObjectURL(image)}
                  alt="selected"
                />
              </div>
            )}
          </div>
          <button type="submit">Enviar</button>
        </form>
      </div>
    </div>
  );
}
